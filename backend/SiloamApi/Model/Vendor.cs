﻿namespace SiloamApi.Model
{
    public class Vendor
    {
        public int vendorId { get; set; }
        public string vendorName { get; set; }

        public string createBy { get; set; }

    }
}
