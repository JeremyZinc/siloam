﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using SiloamApi.Model;
using System.Data;

namespace SiloamApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public VendorController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public JsonResult GetVendor()
        {
            string query = @"select id as ""vendorId"", name as ""vendorName"", createBy from vendor";

            DataTable dt = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("SiloamAppCon");
            NpgsqlDataReader reader;
            using (NpgsqlConnection myConn = new NpgsqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myConn))
                {
                    reader = myCommand.ExecuteReader();
                    dt.Load(reader);

                    reader.Close();
                    myConn.Close();
                }
            }

            return new JsonResult(dt);
        }

        [HttpPost]
        public JsonResult AddVendor(Vendor ven)
        {
            string query = @"insert into vendor (name, createBy, createdate) 
                            values (@vendorName, @createBy, CURRENT_TIMESTAMP) ";

            DataTable dt = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("SiloamAppCon");
            NpgsqlDataReader reader;
            using (NpgsqlConnection myConn = new NpgsqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myConn))
                {
                    myCommand.Parameters.AddWithValue("@vendorName", ven.vendorName);
                    myCommand.Parameters.AddWithValue("@createBy", ven.createBy);
                    reader = myCommand.ExecuteReader();
                    dt.Load(reader);

                    reader.Close();
                    myConn.Close();
                }
            }

            return new JsonResult("Added Successfuly");
        }

        [HttpDelete("{id}")]
        public JsonResult DeleteVendor(int id)
        {
            string query = @"delete from vendor where id = @id";

            DataTable dt = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("SiloamAppCon");
            NpgsqlDataReader reader;
            using (NpgsqlConnection myConn = new NpgsqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myConn))
                {
                    myCommand.Parameters.AddWithValue("@id", id);
                    reader = myCommand.ExecuteReader();
                    dt.Load(reader);

                    reader.Close();
                    myConn.Close();
                }
            }

            return new JsonResult("Deleted Successfuly");
        }

        [HttpPut]
        public JsonResult EditVendor(Vendor ven)
        {
            string query = @"update vendor
                            set name = @vendorName
                            where id = @vendorId";

            DataTable dt = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("SiloamAppCon");
            NpgsqlDataReader reader;
            using (NpgsqlConnection myConn = new NpgsqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myConn))
                {
                    myCommand.Parameters.AddWithValue("@vendorName", ven.vendorName);
                    myCommand.Parameters.AddWithValue("@vendorId", ven.vendorId);
                    reader = myCommand.ExecuteReader();
                    dt.Load(reader);

                    reader.Close();
                    myConn.Close();
                }
            }

            return new JsonResult("Update Successfuly");
        }
    }
}
