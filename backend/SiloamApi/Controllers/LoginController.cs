﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using SiloamApi.Model;
using System.Data;

namespace SiloamApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        public JsonResult Login(Member mem)
        {
            bool isExists = false;
            string query = @"select count(1) as total from userLogin where member = @userName and password = @password";

            DataTable dt = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("SiloamAppCon");
            NpgsqlDataReader reader;
            using (NpgsqlConnection myConn = new NpgsqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myConn))
                {
                    myCommand.Parameters.AddWithValue("@userName", mem.userName);
                    myCommand.Parameters.AddWithValue("@password", mem.password);
                    reader = myCommand.ExecuteReader();
                    dt.Load(reader);

                    isExists = Convert.ToBoolean(dt.Rows[0].ItemArray[0]);

                    reader.Close();
                    myConn.Close();
                }
            }

            if (isExists)
                return new JsonResult("Authorize");
            else
                return new JsonResult("Not Authorize");
        }
    }
}
