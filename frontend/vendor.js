const vendor={template:`
<div>
<button type="button" class="btn btn-primary m-2 fload-end" data-bs-toggle="modal" data-bs-target="#vendorModal" @click="addClick()">Add Vendor</button>

<table class="table table-striped">
<thead>
    <tr>
        <th>
            vendor id
        </th>
        <th>
            vendor Name
        </th>
        <th>
            Action
        </th>
    </tr>
</thead>
<tbody>
    <tr v-for="ven in vendor">
        <td>{{ven.vendorId}}</td>
        <td>{{ven.vendorName}}</td>
        <td>
            <button type="button"
            class="btn btn-light mr-1"
            data-bs-toggle="modal" 
            data-bs-target="#vendorModal" 
            @click="editClick(ven)"
            >
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                </svg>
            </button>
            &nbsp;&nbsp;
            <button type="button"
            class="btn btn-light mr-1"
            data-bs-toggle="modal" 
            data-bs-target="#vendorModal" 
            @click="deleteClick(ven.vendorId)"
            >
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                </svg>
            </button>
        </td>
    </tr>
</tbody>
</table>

<div class="modal fade" id="vendorModal" tabindex="-1" aria-labelledby="vendorModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="vendorModalLabel">{{modalTitle}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="input-group mb-3">
                    <span class="input-group text">Vendor Name</span>
                    <input type="text" class="form-control" v-model="vendorName">
                </div>
                <button type="button" @click="createClick" v-if="vendorId==0" class="btn btn-primary">Create</button>
                <button type="button" @click="updateClick" v-if="vendorId!=0" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>

</div>
`,
data(){
    return{
        vendor:[],
        vendorId:0,
        vendorName:"",
        modalTitle:""
    }
},
methods:{
    refreshData(){
        axios.get(variables.API_URL + "vendor")
        .then((response) => {
            this.vendor = response.data;
        });
    },
    addClick(){
        this.modalTitle = "Add New Vendor";
        this.vendorId = 0;
        this.vendorName = "";
    },
    editClick(ven){
        this.modalTitle = "Modify Vendor";
        this.vendorId = ven.vendorId;
        this.vendorName = ven.vendorName;
    },
    createClick(){
        axios.post(variables.API_URL + "vendor",{
            vendorId:this.vendorId,
            vendorName:this.vendorName,
            createBy: "jeremia" //hardcoded, usually use session
        })
        .then((response)=>{
            this.refreshData();
            alert(response.data);
        })
    },
    updateClick(){
        axios.put(variables.API_URL + "vendor",{
            vendorName:this.vendorName,
            vendorId:this.vendorId,
            createBy: "jeremia" //hardcoded, usually use session
        })
        .then((response)=>{
            this.refreshData();
            alert(response.data);
        })
    },
    deleteClick(id){
        if(!confirm("Are you sure?")){
            return;
        }

        axios.delete(variables.API_URL + "vendor/" + id)
        .then((response)=>{
            this.refreshData();
            alert(response.data);
        })
    }
},
mounted:function(){
    this.refreshData();
}

}
