const routes=[
    {path:'/login', component:login},
    {path:'/home', component:home},
    {path:'/vendor', component:vendor},
    {path:'/logout', component:logout}
]

const isAuthorize = false;
const authName = "";

const router = new VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes
})

const app = Vue.createApp({
    data(){
        return{
            isAuth: isAuthorize,
            isAuthName: authName
        }
    }
})
app.config.globalProperties.$gIsAuth = false;
app.config.globalProperties.$gIsAuthName = "";

app.use(router)

app.mount('#app')