const login={template: `
<div class="row justify-content-md-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">Login</div>
            <div class="card-body">
                <div>
                    <div class="form-group">
                        <label for="userName">Username</label>
                        <input type="text" class="form-control" v-model="userName" placeholder="Username..">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" v-model="password" placeholder="Password..">
                    </div>
                    <div class="form-group">
                        <br/>
                        <button type="button" @click="loginClick" class="btn btn-primary">Login</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
data(){
    return {
        userName:"",
        password:""
    }
},
methods:{
    pageLoad(){
        
    },
    loginClick(){
        axios.post(variables.API_URL + "Login",{
            userName: this.userName,
            password: this.password
        })
        .then((response)=>{
            if (response.data == "Authorize"){
                alert("Wellcome, " + this.userName);
                this.$gIsAuth = true;
            } else {
                alert("Incorrect username or password");
                this.$gIsAuth = false;
            }
        })
    }
},
mounted:function(){
    if(this.$gIsAuth == false){
        this.pageLoad();
    }
    //else {} //if authorize go to home component
}
}